# Cool movies technical test
[![pipeline status](https://gitlab.com/darklight9811/coolmovies/badges/main/pipeline.svg)](https://gitlab.com/darklight9811/coolmovies/-/commits/main) 

> This technical test was provided by [ecoPortal](https://www.ecoportal.com/) for the frontend position.

Welcome to the technical test. Briefly below you can read about main points of this test. You can read more about the test in the `CHALLENGE.md` file.

## Running application
To run the application is considered you have docker and yarn installed. To run the application you must do the following:
- cd to the `apps/backend`
- run `docker-compose up`
- open another terminal
- cd to `apps/frontend`
- run `yarn`
- run `yarn dev`

## Design documents
Design documents are created to publish a pattern or change made to the code, so they can be later easily understood by the team and new members. Formalized, they also help create a discussion environment to enhance their proposals. You can check the design documents in their respective directories.

### Why place them as markdown inside the repo?
- I wanted to centralize everything in the repository;
- They should not be issues because they don't close, they are constant evolving discussions (and since gitlab doesn't have discussions, I opted for this);
- Wiki doesn't allow comments, thread and topics;
- Notion is outside the repository, even with integrations, I would rather keep it in the scope;

## Figma design
Designing a good UI is important because it can affect the user experience in many ways. You can checkout our visual design [here](https://www.figma.com/file/j8joDDHyROmCHUNJ7z7mXN/Untitled?node-id=0%3A1).