// Views
import Home from '../views/Home';

const HomePage = () => {
  // -------------------------------------------------
  // Render
  // -------------------------------------------------

  return (
    <Home />
  );
};

export default Home;
