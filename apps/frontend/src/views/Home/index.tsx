// Packages
import { Paper, Typography } from '@mui/material';

// Component
import styles from "./style"

const Home = () => {
  // -------------------------------------------------
  // Render
  // -------------------------------------------------

  return (
    <div css={styles.root}>
      <Paper elevation={3} css={styles.navBar}>
        <Typography>{'EcoPortal'}</Typography>
      </Paper>

      <div css={styles.body}>
        <Typography variant={'h1'} css={styles.heading}>
          {'EcoPortal Coolmovies Test'}
        </Typography>
        <Typography variant={'subtitle1'} css={styles.subtitle}>
          {`Thank you for taking the time to take our test. We really appreciate it. 
        All the information on what is required can be found in the README at the root of this repo. 
        Please dont spend ages on this and just get through as much of it as you can. 
        Good luck! :) `}
        </Typography>
      </div>
    </div>
  );
}

export default Home