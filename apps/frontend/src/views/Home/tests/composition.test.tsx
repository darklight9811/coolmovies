// Packages
import { render, screen } from '@testing-library/react'

// Components
import Home from '..'

describe('Home tests', () => {
  it('renders without exception', () => {
    const { container } = render(<Home />)

    expect(container).toBeDefined()
  })
})