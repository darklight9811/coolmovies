# Backend

The backend is already handled by the docker image. So we don't have to implement it, only use it. But there are still some points about security and best practices left:

- we should proxy it through the next's API endpoint so we can hide our backend's IP;
- backend IP should be configurable (through an env);
- differential: allow the user to set his own backend endpoint (for testing and because the current one doesn't have any security to be put online);

## Proposal

- allow the user to change the API endpoint;
- popup a request to change in case the endpoint doesn't work;
- the default endpoint is to the next's API endpoint;
- next's API endpoint should be to the env key;
- env key should point to the default localhost:5000;

### Pros
- customizable
- developer friendly: you can use toggle the endpoint to your necessity

### Cons
- It's not actually totally secure, would need more investment on that