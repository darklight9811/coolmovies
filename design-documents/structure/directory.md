# Directory
Directory structuring is crucial to your organization, here we are going to talk some of the main strategies we adopt to prevent faulty file placement. But before that, we will have to introduce some concepts.

## Functional, organizational, resource directories
Unconsciously we create two types of directories in our projects: functional and organizational. But what are the differences between those two? Functional directories separate different scripts or file types (their name describe the function of the file inside), some examples:

- scripts
	- controllers
	- validators
	- models
- styles
	- mixins
	- functions
	- components

And organizational directories are separated for human readability, usually to prevent a functional directory for growing to big. The example below:

- controllers
	- admins
		- statisticsController.ts
		- accountsController.ts
	- users
		- accountsController.ts
		- socialController.ts
		- authController.ts
	- posts
	- comments

Controllers is a functional directory containing the same type of scripts within it's directories, but they are separated by responsability (something more useful for distinguishing what that file is about and narrowing it's scope name). Lastly we have resource directories:

- users
	- validator.ts
	- controller.ts
	- model.ts
- posts
	- validator.ts
	- controller.ts
	- model.ts

Resource directories segregate the file types per it's resource, that is helpful when you have a relationship between those files or you have the same file types for every resource. It's important to note you should not be mixing directories/files with different types, since they could conflict between them.

### How would they conflict?

- controllers
- validators
- models
- routes

By the names you might think it's a functional pattern only? But what if I said that models refer to a resource called topmodels and routes is a related resource to models, containing orm model, validator, controller? If you worked long enough in a project that might not bother you (but still would bother newcomers), but another issue would be that if you had to add a directory named models containing actual ORM model classes, that wouldn't be possible.

> Even thou this seems laughable and easily avoidable. A lot of programmers still commit this mistake

Usually you use functional strategy or resource one, but you can mix them (not in the same context) if it seems necessary, every rule can be broken if it has a good reason for it. But usually you will use one or the another, but how to choose?

**Does the file functionalities share the same resources (controllers/{user, post}, validators/{user, post}, models/{user, post})?**

If yes, you probably should go for the resource strategy, but this is not obligatory. Every framework has it's preferences and you may need adapt on your environment to better fit it's necessities.