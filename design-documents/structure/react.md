# React file types

Even thou most components are identitical in structure, they serve very different purposes. Here we are going to talk about react file types and mostly component types:

## Components
### Views
Views are indenpendant components (you don't need to pass props) and are called directly by the router. They match a URL and are composed of containers and components. Some examples are: UserProfile, UserEdit, Home, NotFound. They pass information to containers such as query params, but are not declared inside them, all functionalities must come from outside.

Views are almost equivalent to controllers in a backend environment, they orchestrate everything but do not act themselves. They gather information and give them to who needs it.

### Containers
They are semi-independant components (they can receive props but this is not necessary) containing business-logic (or connecting with a service in a more complex situation), they don't render nothing themselves but are able to call visual components to do so. Some examples would be: lists and forms.

Containers can be abstract, meaning they can adapt what they show using some conditions, for example, you have a table container, you pass the headers that it should display and it handles the API request, filtering, ordering, etc. Or you could have concrete containers such as user list, that has everything setup and you can only offset the page or pass a filter to the list.

### Components
This can look rendundant, but those are the components responsible for rendering interfaces and are the most basic parts of an application, such: buttons, inputs, modals, headers, etc.

## Store
### Models
Models declare the data structure contained in the store, both interfaces and initial data.

### Services
They are actions that alter the store data.

## Utils
Since this is pretty much self explanatory, we are going to list common directories present in here: hooks (react hooks), request (sometimes handled by a library such as apollo), functional (functional programming methods, they can also be distributed inside other directories if required), math (common math operations, usually common in financial dashboards).