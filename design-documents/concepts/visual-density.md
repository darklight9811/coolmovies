# Visual density

When working on a file, the quantity of information displayed on a screen can end up overwhelming, slowing down the process of finding a specific part of the code. Usually we don't want too much info at the same time, for that, we can use comments, empty spaces and line breaking.

## Why is this a problem?

Complex logic tends to increase visual density by themselves, making even harder than it already is. As a programmer, time is crucial and wasting time finding things should be avoided. Even when programmers get used to the file, there is a certain limit of speed they can achieve with a high density level. And it's worse for someone new, who will take astonishingly longer that he should to find and understand what is happening.

## How to decrease it

### Line breaking

When a line contains too much code, scrolling between parts of it can kill your logic reasoning. So whenever possible, you should break it into multiple lines, this can be easier if you have any dot concatenation, commas and string literals.

### Empty spaces

The most efficient way of decreasing visual density. Separating similar clusters of code by an empty line can do wonders for logical reasoning, following one of the principles of gestalt, things that serve the same purpose should stick together, so you know when something ends and another starts.

### Comments

This is a complicated one, because it depends on how you implement comments in your code. Basically this only helps if:

- they are distributed as headers for each chunk of code (sectional or logical, dividing a whole section of code or just a few lines that form a cluster)
- it's color has a contrast from any other color that represents code and has not too much contrast with the background
- they are not too big

Breaking any of those rules will result in the increase of visual density. Imagine you have a school science book, it doesn't have headers, labels, titles, horizontal rulers. Comments here serve more as a layout component than a functional one (their content doesn't matter as much)