# Contextual naming

The human being is used to work with contexts, you don't need to refer to your brother using his full name, thing that would happen if he were from another family, city or country. And why with variables things need to be different?

If we are inside the context of a table in a database of a user, we don't need to say "id of a user", id explicitly tells us we are talking of the current context. We only let them explicit if they don't belong in the current context.

But why do this if we can let everything go verbose?

Firstly, when we need to do a quick reading, we don't retain information, so something like `setLoggedUserAuthCurrentData` would cause confusion inside our brain, because when we finish reading the variable (when doing a quick eye, not the one you are using to read this article), we forget about information inside the variable.

Secondly, re usage, let's say you have a logic that could not be encapsulated in a inheritance so you can reuse it and you really need to copy and paste what you already done, renaming variables from one resource to antoher is unnecessary and costly, you can just rename it to data or model (that belongs into the current context).

Thirdly, renaming, let's say you have post is going to turn into adminPost, having to rename every reference to the context is not a good idea (even tho most IDEs let you do a name refactor, that is not ideal and may not find 100% of the cases).

Lastly, writing long names increases the chance of a mistake during typing, or consumes a long time writing them. The bigger the character sequence is, the slower we tend to type them (specially new programmers).